module go_test

go 1.18

require (
	github.com/99designs/gqlgen v0.17.5
	github.com/vektah/gqlparser/v2 v2.4.2
	golang.org/x/crypto v0.0.0-20220511200225-c6db032c6c88
)

require (
	github.com/agnivade/levenshtein v1.1.1 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/gorilla/websocket v1.5.0 // indirect
	github.com/hashicorp/golang-lru v0.5.4 // indirect
	github.com/mitchellh/mapstructure v1.3.1 // indirect
)

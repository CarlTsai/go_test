package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"fmt"
	"go_test/graph/generated"
	"go_test/graph/model"
	"strconv"

	"github.com/dgrijalva/jwt-go"
	"golang.org/x/crypto/bcrypt"
)

func (r *mutationResolver) CreateTodo(ctx context.Context, input model.NewTodo) (*model.Todo, error) {
	panic(fmt.Errorf("not implemented???"))
}

func (r *mutationResolver) SignUp(ctx context.Context, name *string, email string, password string) (*model.User, error) {
	// 1. 檢查不能有重複註冊 email
	for _, u := range users {
		if u.Email == email {
			panic(fmt.Errorf("isUserEmailDuplicate "))
		}
	}

	// 2. 將 passwrod 加密再存進去。非常重要 !!
	Password, _ := HashPassword(password)
	// 3. 建立新 user
	newId := strconv.Itoa(len(users) + 1)
	newUser := model.User{newId, *name, email, Password}
	users = append(users, newUser)

	return &newUser, nil
}

func (r *mutationResolver) Login(ctx context.Context, email string, password string) (*model.Token, error) {
	// 1. 透過 email 找到相對應的 user
	var theUser *model.User
	for _, u := range users {
		if email == u.Email {
			theUser = &u
		}
	}

	if theUser == nil {
		panic(fmt.Errorf("Email Account Not Exists"))
	}

	// 2. 將傳進來的 password 與資料庫存的 user.password 做比對
	passwordIsValid := CheckPasswordHash(password, theUser.Password)
	if !passwordIsValid {
		panic(fmt.Errorf("Wrong Password"))
	}

	// 3. 成功則回傳 token
	atClaims := jwt.MapClaims{}
	atClaims["authorized"] = true
	atClaims["user_id"] = theUser.ID
	atClaims["pwd"] = theUser.Password
	//atClaims["exp"] = time.Now().Add(time.Minute * 15).Unix()
	at := jwt.NewWithClaims(jwt.SigningMethodHS256, atClaims)
	token, _ := at.SignedString([]byte("ACCESS_SECRET"))
	return &model.Token{Token: token}, nil
}

func (r *queryResolver) Todos(ctx context.Context) ([]*model.Todo, error) {
	tmp0 := model.Todo{ID: "001", Text: "XDDD", Done: true}
	tmp := []*model.Todo{&tmp0}
	return tmp, nil
}

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

type mutationResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }

// !!! WARNING !!!
// The code below was going to be deleted when updating resolvers. It has been copied here so you have
// one last chance to move it out of harms way if you want. There are two reasons this happens:
//  - When renaming or deleting a resolver the old code will be put in here. You can safely delete
//    it when you're done.
//  - You have helper methods in this file. Move them out to keep these resolver files clean.
var users []model.User = []model.User{
	{
		ID:       "1",
		Email:    "fong@test.com",
		Password: "$2b$04$wcwaquqi5ea1Ho0aKwkZ0e51/RUkg6SGxaumo8fxzILDmcrv4OBIO", // 123456
		Name:     "Fong",
	}, {
		ID:       "2",
		Email:    "kevin@test.com",
		Password: "$2b$04$uy73IdY9HVZrIENuLwZ3k./0azDvlChLyY1ht/73N4YfEZntgChbe", // 123456
		Name:     "Kevin",
	},
}

func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}
func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}
